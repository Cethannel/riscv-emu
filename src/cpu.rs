use crate::{bus::{Bus, DRAM_BASE}, dram::DRAM_SIZE};

pub struct CPU {
    regs: [u32; 32],
    pub pc: u32,
    bus: Bus,
}

impl CPU {
    pub fn new(code: Vec<u8>) -> CPU {
        let mut regs = [0; 32];
        regs[2] = DRAM_BASE + DRAM_SIZE;

        CPU {
            regs,
            pc: DRAM_BASE,
            bus: Bus::new(code),
        }
    }

    fn load(&self, addr: u32, size: u32) -> Result<u32, ()> {
        self.bus.load(addr, size)
    }

    fn store(&mut self, addr: u32, size: u32, value: u32) -> Result<(), ()> {
        self.bus.store(addr, size, value)
    }

    pub fn fetch(&mut self) -> Result<u32, ()> {
        match self.bus.load(self.pc, 32) {
            Ok(inst) => Ok(inst),
            Err(_e) => Err(()),
        }
    }

    pub fn execute(&mut self, inst: u32) {
        let opcode = inst & 0x7f;
        let rd = ((inst >> 7) & 0x1f) as usize;
        let rs1 = ((inst >> 15) & 0x1f) as usize;
        let rs2 = ((inst >> 20) & 0x1f) as usize;
        let func3 = (inst >> 12) & 0x7;
        let func7 = (inst >> 25) & 0x7;

        // println!("Inst is {:#034b}", inst);
        // println!("Opcode is {:#010b}", opcode);

        match opcode {
            0x03 => {
                // Load instruction
                let imm = ((inst as i32) >> 20) as u32;
                let addr = self.regs[rs1].wrapping_add(imm.clone());

                //TODO: MORE HERE

                match func3 {
                    0b000 => {
                        let val = self
                            .load(addr, 8)
                            .expect(format!("Failed to load byte at addr: {:#02x}", addr).as_str());
                        self.regs[rd] = val as i8 as i32 as u32;
                    }
                    0b001 => {
                        self.regs[rd] = self.load(addr, 16).expect(
                            format!("Failed to load half word at addr: {:#02x}", addr).as_str(),
                        );
                    }
                    0b010 => {
                        self.regs[rd] = self
                            .load(addr, 32)
                            .expect(format!("Failed to load word at addr: {:#02x}", addr).as_str());
                    }
                    0b011 => {
                        self.regs[rd] =
                            self.load(addr, 8).expect(
                                format!("Failed to load byte at addr: {:#02x}", addr).as_str(),
                            ) << 16;
                    }
                    0b100 => {
                        self.regs[rd] = self.load(addr, 16).expect(
                            format!("Failed to load half word at addr: {:#02x}", addr).as_str(),
                        ) << 16;
                    }
                    _ => {
                        println!("Failed to match func3: {:#010b}", func3);
                    }
                }
            }
            0x23 => {
                let imm = (((inst & 0xfe000000) as i32 >> 20) as u32) | ((inst >> 7) & 0x1f);
                let addr = self.regs[rs1].wrapping_add(imm);
                match func3 {
                    0b000 => {
                        self.store(addr, 8, self.regs[rs2]).unwrap();
                    }
                    0b001 => {
                        self.store(addr, 16, self.regs[rs2]).unwrap();
                    }
                    0b010 => {
                        self.store(addr, 32, self.regs[rs2]).unwrap();
                    }
                    _ => {
                        println!("Failed to match func3: {:#010b}", func3);
                    }
                }
            }
            0x33 => {
                match func3 {
                    0x0 => {
                        match func7 {
                            0x0 => { // add
                                self.regs[rd] = self.regs[rs1] + self.regs[rs2];
                            }
                            0x20 => { // subtract
                                self.regs[rd] = self.regs[rs1] - self.regs[rs2];
                            }
                            _ => {
                                println!("Failed to match func7: {:#010b}", func7);
                            }
                        }
                    }
                    _ => {
                        println!("Failed to match func3: {:#010b}", func3);
                    }
                }
            }
            0x17 => {
                let imm = inst >> 12;
                self.regs[rd] = self.pc - 4 + (imm << 12);
            }
            0x6f => {
                let imm = inst >> 12;
                self.regs[rd] = self.pc + 4;
                self.pc = imm;
                println!("Changed by: {}", imm);
            }
            _ => {
                println!("Failed to match opcode: {:#010b}", opcode);
            }
        }
    }
}

pub fn print_instructions(code: Vec<u8>) {
    println!("Instrcutions:");
    for i in 0..(code.len()/4) {
        let inst = (code[(i*4)] as u32)
            |((code[(i*4)+1] as u32) << 8)
            |((code[(i*4)+2] as u32) << 16)
            |((code[(i*4)+3] as u32) << 24);
        
        if inst != 0 {
            println!("{:#04x}: {:#034b}", i, inst);
        }
    }

    println!("Last instruction: {:#04x}", code.len());
}
