use std::{fs::File, env, io::Read};

use riscvemu::cpu::CPU;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Please provide source file");
        return;
    }

    let mut file = File::open(&args[1]).expect("Failed to load file");
    let mut code = Vec::new();
    file.read_to_end(&mut code).expect("Failed to read file to buffer");

    // for (index, val) in code.clone().iter().enumerate() {
    //     println!("{:#06x}: {:#010b}",index , val);
    // }

    let mut cpu = CPU::new(code);

    loop {
        let inst = match cpu.fetch() {
            Ok(inst) => inst,
            Err(_) => break,
        };

        cpu.pc += 4;

        cpu.execute(inst);

        if cpu.pc == 0 {
            println!("Exit");
            break;
        }
    }
}
